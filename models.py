from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String

engine = create_engine('sqlite:///jokes.db', echo = True)
meta = MetaData()

jokes = Table(
   'jokes', meta,
   Column('id', Integer, primary_key = True),
   Column('text', String),
)

if __name__ == '__main__':
   meta.create_all(engine)
