# Squadmakers challenge

## Install dependencies

    pipenv install

## Migrate database

    python models.py

## Start the server

    uvicorn main:app

## Run tests

    pytest

## Second question on challenge

- SQL solution:
    1. I would use postgresSQL as it is a good open source solution solution with great support on cloud services and good scalability.
    2. SQL creation command:
```
CREATE TABLE JOKES(
   ID INT PRIMARY KEY     NOT NULL,
   TEXT           TEXT    NOT NULL,
   CREATED_AT     TIMESTAMP NOT NULL,
   UPDATED_AT     TIMESTAMP
);
```
- NoSQL solution:
    1. I would use MongoDB as it is a good open source solution solution with great support on cloud services and good scalability.
    2. NoSQL creation command:
```
db.createCollection("jokes")
```
For this case it would just create the collection and then use the same data model created for the SQL table when inserting data.
