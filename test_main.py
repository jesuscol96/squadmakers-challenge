from fastapi.testclient import TestClient
from main import app

client = TestClient(app)


def test_joke_endpoint_random_joke():
    # Test random joke from db
    response = client.get("/joke")
    assert response.status_code == 200
    data = response.json()
    assert data["source"] == "internal"

def test_joke_endpoint_chuck_joke():
    # Test chuck joke
    response = client.get("/joke/Chuck")
    assert response.status_code == 200
    data = response.json()
    assert data["source"] == "Chuck"

def test_joke_endpoint_dad_joke():
    # Test dad joke
    response = client.get("/joke/Dad")
    assert response.status_code == 200
    data = response.json()
    assert data["source"] == "Dad"

def test_joke_endpoint_create():
    # Test joke creation in db
    response = client.post("/joke/", json={"text": "test joke"}, headers={"Content-Type": "application/json"})
    assert response.status_code == 201
    data = response.json()
    assert data["result"] == "ok"

def test_joke_endpoint_update():
    # Test joke update in db
    response = client.post("/joke/", json={"text": "test joke"}, headers={"Content-Type": "application/json"})
    assert response.status_code == 201
    data = response.json()
    assert data["result"] == "ok"
    number = data["number"]
    response = client.put("/joke/", json={"text": "test joke updated", "number": number}, headers={"Content-Type": "application/json"})
    assert response.status_code == 200
    data = response.json()
    assert data["result"] == "ok"

def test_joke_endpoint_delete():
    # Test joke deletion in db
    response = client.post("/joke/", json={"text": "test joke"}, headers={"Content-Type": "application/json"})
    assert response.status_code == 201
    data = response.json()
    assert data["result"] == "ok"
    number = data["number"]
    response = client.delete("/joke/", json={"number": number}, headers={"Content-Type": "application/json"})
    assert response.status_code == 200
    data = response.json()
    assert data["result"] == "ok"

def test_math_endpoint_lcm():
    # Test math endpoint: lcm
    response = client.get("/math/lcm?numbers=5&numbers=4&numbers=2")
    assert response.status_code == 200
    data = response.json()
    assert data["lcm"] == 20

def test_math_endpoint_plus_one():
    # Test math endpoint: plus one
    response = client.get("/math/plus_one?number=64")
    assert response.status_code == 200
    data = response.json()
    assert data["plus_one"] == 65
