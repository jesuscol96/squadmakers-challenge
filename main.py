import math
import requests
from typing import List, Union
from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String
from fastapi import FastAPI, status, HTTPException, Query
from pydantic import BaseModel
from models import engine, jokes

app = FastAPI()


CHUCK_API = "https://api.chucknorris.io/"
DAD_API = "https://icanhazdadjoke.com/"
CHUCK_JOKE_TYPE = "Chuck"
DAD_JOKE_TYPE = "Dad"


class Joke(BaseModel):
    number: int = None
    text: str

class JokeNumber(BaseModel):
    number: int = None


@app.get("/joke/{joke_type:path}", status_code=status.HTTP_200_OK)
async def joke(joke_type):
    if not joke_type:
        joke = "No jokes found."
        with engine.connect() as conn:
            fetch_jokes = jokes.select()
            result = conn.execute(fetch_jokes)
            row = result.fetchone()
            if row:
                joke = row[1]
        return {"source": "internal", "joke": joke}

    joke_key = None
    headers = {"Accept": "application/json"}
    if joke_type == CHUCK_JOKE_TYPE:
        response = requests.get(f"{CHUCK_API}/jokes/random")
        joke_key = "value"
    if joke_type == DAD_JOKE_TYPE:
        response = requests.get(DAD_API, headers=headers)
        joke_key = "joke"

    if response:
        if not response.ok:
            raise HTTPException(status_code=500, detail="Source unavailable.")
        data = response.json()
        return {"source": joke_type, "joke":  data[joke_key]}

    raise HTTPException(status_code=400, detail="Not valid joke type.")

@app.post("/joke/", status_code=status.HTTP_201_CREATED)
async def joke(joke: Joke):
    result = None
    with engine.connect() as conn:
        ins = jokes.insert().values(text=joke.text)
        result = conn.execute(ins)
    if result:
        return {"result": "ok", "number": result.inserted_primary_key[0]}
    raise HTTPException(status_code=400, detail="Joke creation failed.")

@app.put("/joke/", status_code=status.HTTP_200_OK)
async def joke(joke: Joke):
    result = None
    with engine.connect() as conn:
        update = jokes.update().where(jokes.c.id == joke.number).values(text=joke.text)
        result = conn.execute(update)
    if result:
        return {"result": "ok"}
    raise HTTPException(status_code=400, detail="Joke update failed.")

@app.delete("/joke/", status_code=status.HTTP_200_OK)
async def joke(joke: JokeNumber):
    result = None
    with engine.connect() as conn:
        update = jokes.delete().where(jokes.c.id == joke.number)
        result = conn.execute(update)
    if result:
        return {"result": "ok"}
    raise HTTPException(status_code=400, detail="Joke deletion failed.")


# Math endpoint least common multiple
@app.get("/math/lcm", status_code=status.HTTP_200_OK)
async def numbers_lcm(numbers: List[int] = Query(default=[])):
    lcm = math.lcm(*numbers)
    return {"lcm": lcm}

# Math endpoint plus one
@app.get("/math/plus_one", status_code=status.HTTP_200_OK)
async def numbers_lcm(number: int):
    return {"plus_one": number + 1}
